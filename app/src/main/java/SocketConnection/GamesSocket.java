package SocketConnection;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import HelpClasses.IdWidgets;
import HelpClasses.Widget;

public class GamesSocket {
    PrintWriter pw;
    Scanner scan;
    public ArrayList<Widget> widList;
    int id;
    Socket s;
    SocketAction sa;


    public GamesSocket(int idP, SocketAction saP, final Context context) throws IOException {
        id = idP;
        sa = saP;
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    s = new Socket(IpConfig.ip,2282);
                    widList = new ArrayList<Widget>();
                    pw = new PrintWriter(new OutputStreamWriter(s.getOutputStream(),"UTF-8"));
                    scan = new Scanner(s.getInputStream(),"UTF-8");
                    pw.println("i" + id);
                    pw.flush();

                    Widget old = new Widget(-1,"~","~");
                    while(scan.hasNext()){
                        String data = scan.nextLine();
                        System.out.println("data "+data);
                        switch (data.charAt(0)){
                            case 'a': old.a = data.substring(1);
                                break;
                            case 'b':
                                if(old.id == IdWidgets.TEXT_IMAGE){
                                    System.out.println(data);
                                    old.b = id + "_" + data.substring(1)+".jpg";
                                    String img = "";
                                    while(scan.hasNextLine()){
                                        String part = scan.nextLine();
                                        if(part.equals("end")){
                                            break;
                                        }
                                        img += part;
                                    }
                                    System.out.println("img "+img);
                                    byte [] encodeByte=Base64.decode(img,Base64.DEFAULT);
                                    OutputStream fos = context.openFileOutput(old.b, Context.MODE_PRIVATE);
                                    fos.write(encodeByte);
                                    fos.flush();
                                    fos.close();
                                }else{
                                    old.b = data.substring(1);
                                }
                                break;
                            case 'i': old.id = Integer.parseInt(data.substring(1));
                                break;
                        }
                        System.out.println(old.a + " " + old.b + " " + old.id);
                        if(old.a != "~" && old.b != "~" && old.id != -1){
                            widList.add(old);
                            old = new Widget(-1,"~","~");
                        }
                    }
                    System.out.println("onFull");
                    //   scan.close();
                    sa.OnFull();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


    }
}
