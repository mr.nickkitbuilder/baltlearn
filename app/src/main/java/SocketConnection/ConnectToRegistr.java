package SocketConnection;

import android.content.Intent;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class ConnectToRegistr {
    PrintWriter pw;
    Scanner scan;

    public ConnectToRegistr(Socket s, final String nick, final String pass, final int id, final RegAction r) throws IOException {
        pw = new PrintWriter(new OutputStreamWriter(s.getOutputStream(),"UTF-8"));
        scan = new Scanner(s.getInputStream(),"UTF-8");
        pw.println(String.valueOf(id));
        pw.println(nick);
        pw.println(pass);
        pw.flush();
        pw.close();
        if(scan.hasNextLine()) {
            String answer = scan.nextLine();
            System.out.println(answer);
            r.serverAnswer(answer);
        }
        scan.close();
    }
}
