package SocketConnection;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import HelpClasses.AuthorSettings;
import HelpClasses.PreviewGamesValues;

public class MainActivitySocket {

    ArrayList<PreviewGamesValues> pgvList;

    public ArrayList<PreviewGamesValues> getPgvList() {
        return pgvList;
    }

    public MainActivitySocket(final SocketAction socketAction, final int mode, final String key, final int out) {
        pgvList = new ArrayList<>();

        final Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Socket sock;
                    if(mode == 2){
                        sock =  new Socket(IpConfig.ip,2284);
                    }else{
                        sock = new Socket(IpConfig.ip,2281);
                    }
                    PrintWriter pw = new PrintWriter(new OutputStreamWriter(sock.getOutputStream(),"UTF-8"));
                    Scanner scan = new Scanner(sock.getInputStream(),"UTF-8");
                    System.out.println("mode " + mode);
                    if(mode == 0) {
                        System.out.println("чиселка "+Settings.installFiles);
                        pw.println("n" + out +" " +(Settings.installFiles + out));
                        pw.flush();
                    }
                    if(mode == 1) {
                        pw.println("i" + AuthorSettings.nick);
                        System.out.println("nick " + AuthorSettings.nick);
                        pw.flush();
                    }
                    if(mode == 2) {
                        pw.println(key);
                        pw.println("10");
                        pw.flush();
                    }
                    PreviewGamesValues pgv = new PreviewGamesValues("", "", -1, "");
                    while (scan.hasNext()) {
                        String in = scan.nextLine();
                        switch (in.charAt(0)) {
                            case 'i':
                                pgv.id = Integer.parseInt(in.substring(1));
                                System.out.println("id " + pgv.id);
                                break;
                            case 'n':
                                pgv.a = in.substring(1);
                                break;
                            case 'd':
                                pgv.b = in.substring(1);
                                break;
                            case 'c':
                                pgv.creator = in.substring(1);
                                break;
                        }
                        if (pgv.a != "" && pgv.b != "" && pgv.id != -1 && pgv.creator != "") {
                            pgvList.add(pgv);
                            pgv = new PreviewGamesValues("", "", -1, "");
                        }
                    }

                    scan.close();
                    sock.close();
                    socketAction.OnFull();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
        th.start();
    }
}
