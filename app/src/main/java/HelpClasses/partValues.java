package HelpClasses;

public class partValues {
    private int image;
    private String str;
    private int id;

    public partValues(int image, String str, int id) {
        this.image = image;
        this.str = str;
        this.id = id;

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {

        return id;
    }


    public int getImage() {
        return image;
    }

    public String getStr() {
        return str;
    }

    public void setImage(int image) {

        this.image = image;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
