package HelpClasses;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.soft.nick.learn.R;

public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.WidgetViewHolder>{
    ListOfWidgets low;
    Context mContext;
    public TimelineAdapter(ListOfWidgets low, Context context) {
        this.low = low;
        this.mContext = context;
    }


    @NonNull
    @Override
    public WidgetViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.timeline_part,viewGroup,false);
        WidgetViewHolder wh = new WidgetViewHolder(v);
        return wh;
    }

    @Override
    public void onBindViewHolder(@NonNull final WidgetViewHolder widgetViewHolder, int i) {
        Widget pv = low.widgets.get(i);
        switch (pv.id){
            case IdWidgets.TEXT_TEXT:widgetViewHolder.image.setImageResource(R.drawable.text_text);
                break;
            case IdWidgets.TEXT_LETTERS:widgetViewHolder.image.setImageResource(R.drawable.text_letters);
                break;
            case IdWidgets.TEXT_ANSWERS:widgetViewHolder.image.setImageResource(R.drawable.text_answers);
                break;
            case IdWidgets.TEXT_IMAGE:widgetViewHolder.image.setImageResource(R.drawable.text_image);
                break;

        }

        widgetViewHolder.name.setText(pv.a);
    }



    @Override
    public int getItemCount() {
        return low.widgets.size();
    }

    public static class WidgetViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{
        public ImageView image;
        public TextView name;
        public RelativeLayout rl;

        public WidgetViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageViewT);
            name = itemView.findViewById(R.id.textOfWidT);
            rl = itemView.findViewById(R.id.relativeT);
            rl.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Settings");
            menu.add(this.getAdapterPosition(),0,0,"Delete");
        }
    }
}
