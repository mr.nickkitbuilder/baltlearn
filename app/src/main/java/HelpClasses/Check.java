package HelpClasses;

public class Check {
    public static boolean hasChars(String s){
        try {
            Integer.parseInt(s);
        }catch(Exception e){
            return true;
        }
        return false;
    }
    public static int getRight(String s){
        int result = 0;
        for(int i = 0;i < s.length();i++){
            char old = s.charAt(i);
            if(old == ')') {
                break;
            }
            result += Integer.parseInt(String.valueOf(old)) * Math.pow(10,i);
        }
        return result;
    }
    public static int getCountsOfDigits(int n) {
        if (n < 100000) {
            if (n < 100) {
                if (n < 10) {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                if (n < 1000) {
                    return 3;
                } else {
                    if (n < 10000) {
                        return 4;
                    } else {
                        return 5;
                    }
                }
            }
        } else {
            if (n < 10000000) {
                if (n < 1000000) {
                    return 6;
                } else {
                    return 7;
                }
            } else {
                if (n < 100000000) {
                    return 8;
                } else {
                    if (n < 1000000000) {
                        return 9;
                    } else {
                        return 10;
                    }
                }
            }
        }
    }
}
