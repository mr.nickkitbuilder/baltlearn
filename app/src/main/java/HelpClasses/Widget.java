package HelpClasses;

import android.os.Parcel;
import android.os.Parcelable;

public class Widget implements Parcelable {
    public int id;
    public String a;
    public String b;

    public Widget(int id, String a, String b) {
        this.id = id;
        this.a = a;
        this.b = b;
    }

    protected Widget(Parcel in) {
        id = in.readInt();
        a = in.readString();
        b = in.readString();
    }

    public static final Creator<Widget> CREATOR = new Creator<Widget>() {
        @Override
        public Widget createFromParcel(Parcel in) {
            return new Widget(in);
        }

        @Override
        public Widget[] newArray(int size) {
            return new Widget[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(a);
        parcel.writeString(b);
    }
}
