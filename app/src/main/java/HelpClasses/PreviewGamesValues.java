package HelpClasses;

import java.io.Serializable;

public class PreviewGamesValues implements Serializable {
    public String a;
    public String b;
    public int id;
    public String creator;

    public PreviewGamesValues(String a, String b, int id, String creator) {
        this.a = a;
        this.b = b;
        this.id = id;
        this.creator = creator;
    }

    public PreviewGamesValues(String a, String b, int id) {
        this.a = a;
        this.b = b;
        this.id = id;
        this.creator = "unnamed";
    }


}
