package HelpClasses;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.soft.nick.learn.AddWidgetActivity;
import com.soft.nick.learn.R;

import java.util.ArrayList;

public class WidgetAdapter extends RecyclerView.Adapter<WidgetAdapter.WidgetViewHolder> {

    public ArrayList<partValues> pw;
    private Context context;


    @NonNull
    @Override
    public WidgetViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.partofrecyclerwid,viewGroup,false);
        WidgetViewHolder wh = new WidgetViewHolder(v);
        return wh;

    }

    public WidgetAdapter(ArrayList<partValues> pw){
        this.pw = pw;
    }

    @Override
    public void onBindViewHolder(@NonNull final WidgetViewHolder widgetViewHolder, final int i) {
        partValues pv = pw.get(i);
        widgetViewHolder.image.setImageResource(pv.getImage());
        widgetViewHolder.name.setText(pv.getStr());
        widgetViewHolder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(context,AddWidgetActivity.class);
                        intent.putExtra("id", pw.get(i).getId());
                        ((Activity) context).startActivityForResult(intent,RequestCodes.REQUEST_CODE_FOR_ADD_WIDGET);
                    }
                }).start();
            }
        });
    }

    @Override
    public int getItemCount() {
        return pw.size();
    }

    public static class WidgetViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        public TextView name;
        public RelativeLayout rl;
        public WidgetViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imageView);
            name = itemView.findViewById(R.id.textOfWid);
            rl = itemView.findViewById(R.id.relative);
        }
    }
}
