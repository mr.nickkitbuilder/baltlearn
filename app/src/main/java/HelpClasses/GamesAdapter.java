package HelpClasses;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.soft.nick.learn.GamesInflatorActivity;
import com.soft.nick.learn.MainActivity;
import com.soft.nick.learn.R;

import java.util.ArrayList;

public class GamesAdapter extends RecyclerView.Adapter<GamesAdapter.WidgetViewHolder>{
    public ArrayList<PreviewGamesValues> pgvList;
    private Context context;


    public GamesAdapter(ArrayList<PreviewGamesValues> pgvList) {
        this.pgvList = pgvList;
    }

    @NonNull
    @Override
    public WidgetViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        View v= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.games_adapter,viewGroup,false);
        GamesAdapter.WidgetViewHolder wh = new GamesAdapter.WidgetViewHolder(v);
        return wh;
    }

    @Override
    public void onBindViewHolder(@NonNull final WidgetViewHolder widgetViewHolder, final int i) {
        PreviewGamesValues pgv = pgvList.get(i);
        widgetViewHolder.name.setText(pgv.a);
        widgetViewHolder.des.setText(pgv.b);
        widgetViewHolder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,GamesInflatorActivity.class);
                intent.putExtra("id",pgvList.get(i).id);
                System.out.println("idd" + pgvList.get(i).id);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pgvList.size();
    }

    public static class WidgetViewHolder extends RecyclerView.ViewHolder{
        public TextView name;
        public TextView des;
        public LinearLayout ll;

        public WidgetViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameG);
            des = itemView.findViewById(R.id.desG);
            ll = itemView.findViewById(R.id.llG);
        }
    }
}
