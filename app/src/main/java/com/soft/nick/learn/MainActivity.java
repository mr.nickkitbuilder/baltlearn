package com.soft.nick.learn;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;


import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import HelpClasses.GamesAdapter;
import HelpClasses.RequestCodes;
import SocketConnection.IpConfig;
import HelpClasses.PreviewGamesValues;
import SocketConnection.MainActivitySocket;
import SocketConnection.Settings;

public class MainActivity extends FragmentActivity {
    RecyclerView games;
    ArrayList<PreviewGamesValues> pgvList;
    GamesAdapter ad;
    FloatingActionButton addBut;
    int out = 0, scrollY = 0;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Bundle bundle = data.getExtras();
        if(requestCode == RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER && resultCode == RESULT_OK){
            pgvList = (ArrayList<PreviewGamesValues>) bundle.getSerializable("list");
            System.out.println("2 " + pgvList.size());
            addBut = (FloatingActionButton) findViewById(R.id.button3);
            addBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(MainActivity.this, createActivity.class);
                    startActivity(in);
                }
            });
            games = findViewById(R.id.recyclerGM);
            games.setHasFixedSize(true);
            LinearLayoutManager verticalKayoutManager = new LinearLayoutManager(getApplicationContext());
            games.setLayoutManager(verticalKayoutManager);
            ad = new GamesAdapter(pgvList);
            games.setAdapter(ad);
            games.setOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(newState == RecyclerView.SCROLL_STATE_SETTLING){
                        out += Settings.installFiles;
                        Intent in = new Intent(MainActivity.this,MainSocketActivity.class);
                        in.putExtra("mode",0);
                        in.putExtra("out",out);
                        startActivityForResult(in,RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER);
                    }
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                }
            });

        }
        if(requestCode == RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER && resultCode == 2){
            ArrayList<PreviewGamesValues> newPgvList = (ArrayList<PreviewGamesValues>) bundle.getSerializable("list");
            for(int i = 0; i < newPgvList.size(); i++)
                pgvList.add(newPgvList.get(i));
            ad.notifyDataSetChanged();

            System.out.println("lalallal");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        pgvList = new ArrayList<>();
        Intent in = new Intent(MainActivity.this,MainSocketActivity.class);
        in.putExtra("mode",0);
        in.putExtra("out",out);
        startActivityForResult(in,RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER);
    }
}
