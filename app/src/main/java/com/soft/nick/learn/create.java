package com.soft.nick.learn;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import HelpClasses.AuthorSettings;
import HelpClasses.IdWidgets;
import HelpClasses.partValues;
import SocketConnection.IpConfig;
import HelpClasses.ListOfWidgets;
import HelpClasses.RequestCodes;
import HelpClasses.TimelineAdapter;
import HelpClasses.Widget;
import HelpClasses.WidgetAdapter;

public class create extends AppCompatActivity {
    private RecyclerView widgets;
    private RecyclerView timeline;
    private RecyclerView.LayoutManager verticakKayoutManager;
    private RecyclerView.LayoutManager horizontakLayoutManager;
    private RecyclerView.Adapter ad;
    private ListOfWidgets low;
    Button okB;
    ArrayList<partValues> pw;
    String nameA;
    String desA;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK) {
            switch (requestCode) {
                case RequestCodes.REQUEST_CODE_FOR_ADD_WIDGET:
                    Bundle bundle = data.getExtras();
                    low.widgets.add(new Widget(bundle.getInt("id"), bundle.getString("a"), bundle.getString("b")));
                    ad.notifyDataSetChanged();
                    break;
            }
        }else{
           // Toast.makeText(this,"Error!",Toast.LENGTH_SHORT);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create2);
        low = new ListOfWidgets();
        widgets = findViewById(R.id.WIdgets);
        timeline = findViewById(R.id.Timeline);
        okB = findViewById(R.id.sendB);
        nameA = getIntent().getStringExtra("name");
        desA = getIntent().getStringExtra("des");
        okB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(low.widgets.size() > 0) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Socket socket = new Socket(IpConfig.ip, 2280);
                                PrintWriter ous = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(),"UTF-8"));

                                ous.println("n" + nameA);
                                ous.println("d" + desA);
                                ous.println("c" + AuthorSettings.nick);
                                ous.println("s" + low.widgets.size());
                                for (int i = 0; i < low.widgets.size(); i++) {
                                    ous.println("i" + String.valueOf(low.widgets.get(i).id));
                                    ous.println("a" + low.widgets.get(i).a);
                                    if(low.widgets.get(i).id == IdWidgets.TEXT_IMAGE){
                                        Bitmap bm = BitmapFactory.decodeFile(low.widgets.get(i).b);
                                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
                                        byte[] b = baos.toByteArray();
                                        ous.println("b" + i);
                                        System.out.println("here");
                                        String encodedImg = Base64.encodeToString(b, Base64.DEFAULT);
                                        System.out.println("encImg " + encodedImg);
                                        for(int j = 0; j < Math.ceil(encodedImg.length() / 50000.0f);j++){
                                            if(j == Math.ceil(encodedImg.length() / 50000.0f) - 1)
                                                ous.println(encodedImg.substring(j*50000));
                                            else
                                                ous.println(encodedImg.substring(j*50000,(j+1) * 50000));
                                            ous.flush();
                                        }
                                        ous.println("end");
                                        System.out.println("here here");
                                    }else
                                        ous.println("b" + low.widgets.get(i).b);
                                }

                                System.out.println("here here here");
                                ous.flush();

                                System.out.println("here here here here");
                                ous.close();
                                socket.close();

                                Intent in = new Intent(create.this, MainActivity.class);
                                startActivity(in);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }).start();
                }
            }
        });


        pw = new ArrayList<>();
        pw.add(new partValues(R.drawable.text_text,"Text and text",IdWidgets.TEXT_TEXT));
        pw.add(new partValues(R.drawable.text_letters,"Word and letters",IdWidgets.TEXT_LETTERS));
        pw.add(new partValues(R.drawable.text_answers,"Text and answers",IdWidgets.TEXT_ANSWERS));
        pw.add(new partValues(R.drawable.text_image,"Text and image",IdWidgets.TEXT_IMAGE));



        widgets.setHasFixedSize(true);
        verticakKayoutManager = new LinearLayoutManager(getApplicationContext());
        widgets.setLayoutManager(verticakKayoutManager);
        ad = new WidgetAdapter(pw);
        widgets.setAdapter(ad);


        horizontakLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL,false);
        timeline.setLayoutManager(horizontakLayoutManager);
        ad = new TimelineAdapter(low,this);
        timeline.setAdapter(ad);



    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        System.out.println(low.widgets.get(item.getGroupId()).a);
        low.widgets.remove(item.getGroupId());
        ad.notifyDataSetChanged();
        return super.onContextItemSelected(item);
    }
}
