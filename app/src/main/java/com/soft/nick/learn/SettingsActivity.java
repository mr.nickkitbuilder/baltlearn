package com.soft.nick.learn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import SocketConnection.Settings;

public class SettingsActivity extends AppCompatActivity {
    TextView tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Button exit = findViewById(R.id.exitButt);
        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PrintWriter pw = new PrintWriter(openFileOutput("reg.txt", MODE_PRIVATE));
                    pw.close();
                    Intent intent = new Intent(SettingsActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        SeekBar sb = findViewById(R.id.seekBar);
        tv = findViewById(R.id.textOfSB);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tv.setText(Integer.toString(progress));
                Settings.installFiles = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        sb.setProgress(Settings.installFiles);
    }
}
