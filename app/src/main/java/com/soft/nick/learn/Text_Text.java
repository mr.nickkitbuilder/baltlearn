package com.soft.nick.learn;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Text_Text extends AppCompatActivity {

    Button ok;
    TextView name;
    TextView des;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text__text);
        ok = findViewById(R.id.buttonT_T);
        name = findViewById(R.id.nameT_T);
        des = findViewById(R.id.desT_T);

        String nameS = getIntent().getStringExtra("text");
        String desS = getIntent().getStringExtra("des");

        name.setText(nameS);
        des.setText(desS);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}
