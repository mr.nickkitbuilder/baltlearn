package com.soft.nick.learn.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;

import com.soft.nick.learn.AddWidgetActivity;
import com.soft.nick.learn.MainActivity;
import com.soft.nick.learn.MyGamesActivity;
import com.soft.nick.learn.R;
import com.soft.nick.learn.SearchActivity;
import com.soft.nick.learn.SettingsActivity;


public class ActivityPanel extends Fragment {
    ImageView[] icons;
    Activity act;
    int DuringI;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_activity_panel, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        act = getActivity();

        icons = new ImageView[4];
        icons[0] = act.findViewById(R.id.trendingBut);
        icons[1] = act.findViewById(R.id.myBut);
        icons[2] = act.findViewById(R.id.searchBut);
        icons[3] = act.findViewById(R.id.setBut);


        String actName = act.getLocalClassName();
        System.out.println(actName);
        switch (actName){
            case "MainActivity":
                icons[0].setImageResource(R.drawable.ic_trending_up_bblack_24dp);
                DuringI = 0;
                break;
            case "MyGamesActivity":
                icons[1].setImageResource(R.drawable.ic_account_circle_bblack_24dp);
                DuringI = 1;
                break;
            case "SearchActivity":
                icons[2].setImageResource(R.drawable.ic_search_bblack_24dp);
                DuringI = 2;
                break;
            case "SettingsActivity":
                icons[3].setImageResource(R.drawable.ic_settings_bblack_24dp);
                DuringI = 3;
                break;
        }

        icons[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DuringI != 0){
                    Intent intent = new Intent(act.getApplicationContext(),MainActivity.class);
                    startActivity(intent);
                }
            }
        });
        icons[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DuringI != 1){
                    Intent intent = new Intent(act.getApplicationContext(),MyGamesActivity.class);
                    startActivity(intent);
                }
            }
        });

        icons[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DuringI != 2){
                    Intent intent = new Intent(act.getApplicationContext(),SearchActivity.class);
                    startActivity(intent);
                }
            }
        });

        icons[3].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DuringI != 3){
                    Intent intent = new Intent(act.getApplicationContext(),SettingsActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
