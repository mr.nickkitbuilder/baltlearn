package com.soft.nick.learn;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.TranslateAnimation;

import java.util.ArrayList;

import HelpClasses.AuthorSettings;
import HelpClasses.GamesAdapter;
import HelpClasses.PreviewGamesValues;
import HelpClasses.RequestCodes;

public class MyGamesActivity extends AppCompatActivity {

    RecyclerView games;
    ArrayList<PreviewGamesValues> pgvList;
    GamesAdapter ad;
    FloatingActionButton addBut;
    TranslateAnimation ta;
    Toolbar tb;




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Bundle bundle = data.getExtras();
        if(requestCode == RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER && resultCode == RESULT_OK){
            pgvList = (ArrayList<PreviewGamesValues>) bundle.getSerializable("list");
            addBut = (FloatingActionButton) findViewById(R.id.buttonYG);
            addBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(MyGamesActivity.this, createActivity.class);
                    startActivity(in);
                }
            });
            games = findViewById(R.id.recyclerGMY);
            games.setHasFixedSize(true);
            LinearLayoutManager verticalKayoutManager = new LinearLayoutManager(getApplicationContext());
            games.setLayoutManager(verticalKayoutManager);
            ad = new GamesAdapter(pgvList);
            games.setAdapter(ad);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_games);

        pgvList = new ArrayList<>();
        Intent in = new Intent(MyGamesActivity.this,MainSocketActivity.class);
        in.putExtra("mode",1);
        startActivityForResult(in,RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER);
    }
}
