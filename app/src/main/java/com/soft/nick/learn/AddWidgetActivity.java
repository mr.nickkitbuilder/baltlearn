package com.soft.nick.learn;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

import HelpClasses.Check;
import HelpClasses.IdWidgets;
import HelpClasses.Widget;

public class AddWidgetActivity extends AppCompatActivity {
    private EditText name;
    private EditText des;
    private EditText right;
    ImageView previewBM;
    private int id;
    Widget widget;
    private static final int PERMISSION_REQUEST_CODE = 1;
    Button button;

    private void initFileManager() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            button = findViewById(R.id.pickButt);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i,100);
                }
            });
        } else {
            requestPermissions();
        }
    }
    private void requestPermissions() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                PERMISSION_REQUEST_CODE
        );
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i("TAG", "Permission granted!");
                initFileManager();
            } else {
                Log.i("TAG", "Permission denied");
                requestPermissions(); // Запрашиваем ещё раз
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode){
            case 100:
                if(resultCode == RESULT_OK){
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage,filePathColumn,null,null,null);
                    cursor.moveToFirst();
                    int cloumnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String path = cursor.getString(cloumnIndex);
                    cursor.close();
                    previewBM.setImageBitmap(BitmapFactory.decodeFile(path));
                    widget = new Widget(id,"","");
                    widget.b = path;
                }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getIntent().getExtras();
        id = (int)arguments.get("id");
        if(id == IdWidgets.TEXT_IMAGE){
            widget = new Widget(3,"","");
            setContentView(R.layout.activity_add_widget_with_image);
            previewBM = findViewById(R.id.previewBT);
            initFileManager();
        }else {
            setContentView(R.layout.activity_add_widget);
            des = findViewById(R.id.AddDescriptionA2);
            right = findViewById(R.id.RightId);
        }
        Button but = findViewById(R.id.buttonOkA);
        name = findViewById(R.id.AddDescriptionA);
        switch (id){
            case IdWidgets.TEXT_LETTERS: des.setVisibility(View.INVISIBLE); name.setHint("Enter a name of a word");right.setVisibility(View.INVISIBLE);
                break;
            case IdWidgets.TEXT_TEXT:  des.setVisibility(View.VISIBLE); name.setHint("Set name of theme"); des.setHint("Enter theme description");right.setVisibility(View.INVISIBLE);
                break;
            case IdWidgets.TEXT_ANSWERS: des.setVisibility(View.VISIBLE); name.setHint("Set name of theme"); des.setHint("Enter theme answers"); right.setVisibility(View.VISIBLE);
                break;
        }

    }
    public void onPresented(View v){

        if(name.getText().toString().equals("")){
            Toast.makeText(getApplicationContext(),"Fill name please",Toast.LENGTH_SHORT).show();
        }
        else if(id != IdWidgets.TEXT_IMAGE && des.getText().toString().equals("") && des.getVisibility() != View.INVISIBLE){
            Toast.makeText(getApplicationContext(),"Fill description please",Toast.LENGTH_SHORT).show();
        }
        else if(name.getText().toString().length() > 50){
            Toast.makeText(getApplicationContext(),"You must fill name until 50 symbols",Toast.LENGTH_SHORT).show();
        }
        else if(id != IdWidgets.TEXT_IMAGE &&des.getText().toString().length() > 300){
            Toast.makeText(getApplicationContext(),"You must fill description until 300 symbols",Toast.LENGTH_SHORT).show();
        }
        else if(id != IdWidgets.TEXT_IMAGE && right.getText().toString().equals("") && right.getVisibility() != View.INVISIBLE){
            Toast.makeText(getApplicationContext(),"Fill id please",Toast.LENGTH_SHORT).show();
        }else if(id != IdWidgets.TEXT_IMAGE &&Check.hasChars(right.getText().toString()) && right.getVisibility() != View.INVISIBLE) {
            Toast.makeText(getApplicationContext(), "Fill only numbers please", Toast.LENGTH_SHORT).show();
        }else if(id == IdWidgets.TEXT_IMAGE && widget.b == ""){
            Toast.makeText(getApplicationContext(), "Fill image please", Toast.LENGTH_SHORT).show();
        } else{
            switch (id){
                case IdWidgets.TEXT_LETTERS:
                    widget = new Widget(id,name.getText().toString(),"");
                    break;
                case IdWidgets.TEXT_TEXT:
                    widget = new Widget(id,name.getText().toString(),des.getText().toString());
                    break;
                case IdWidgets.TEXT_ANSWERS:
                    widget = new Widget(id,name.getText().toString(),right.getText() + ")" +des.getText().toString());
                    break;
                case IdWidgets.TEXT_IMAGE:
                    widget.a = name.getText().toString();
                    break;
            }
            Intent in = new Intent();
            in.putExtra("id", widget.id);
            in.putExtra("a",widget.a);
            in.putExtra("b",widget.b);
            setResult(RESULT_OK,in);
            finish();
        }
    }

}
