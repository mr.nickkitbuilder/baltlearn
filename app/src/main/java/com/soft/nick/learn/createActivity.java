package com.soft.nick.learn;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class createActivity extends AppCompatActivity {

    EditText name;
    EditText des;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        Button but = findViewById(R.id.buttonOkC);
        name = findViewById(R.id.editText);
        des = findViewById(R.id.editText2);
        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Fill name please",Toast.LENGTH_SHORT).show();
                }
                else if(des.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"Fill description please",Toast.LENGTH_SHORT).show();
                }
                else if(name.getText().toString().length() > 50){
                    Toast.makeText(getApplicationContext(),"You must fill name until 50 symbols",Toast.LENGTH_SHORT).show();
                }
                else if(des.getText().toString().length() > 300){
                    Toast.makeText(getApplicationContext(),"You must fill description until 300 symbols",Toast.LENGTH_SHORT).show();
                }
                else if(name.getText().toString().charAt(0) == ' ' || des.getText().toString().charAt(0) == ' '){
                    Toast.makeText(getApplicationContext(),"You must not use space like a first symbol",Toast.LENGTH_SHORT).show();
                }
                else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            Intent in = new Intent(createActivity.this, create.class);
                            in.putExtra("name", name.getText().toString());
                            in.putExtra("des", des.getText().toString());
                            startActivity(in);
                        }
                    }).start();
                }
            }
        });
    }

}
