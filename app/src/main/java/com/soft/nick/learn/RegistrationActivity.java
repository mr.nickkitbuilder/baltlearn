package com.soft.nick.learn;

import android.content.Intent;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

import HelpClasses.AuthorSettings;
import SocketConnection.ConnectToRegistr;
import SocketConnection.IpConfig;
import SocketConnection.RegAction;
import SocketConnection.SocketAction;

public class RegistrationActivity extends AppCompatActivity {
    EditText nick;
    EditText pass;
    Button reg;
    Button enter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        readFromFile();
        setContentView(R.layout.activity_registration);
        nick = findViewById(R.id.regEditText);
        pass = findViewById(R.id.passEditText);
        reg = findViewById(R.id.RegBut);
        enter = findViewById(R.id.EnterBut);
        reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String answer = checkData(0,nick.getText().toString(),convPass(pass.getText().toString()));
                            if(answer.charAt(0) == 's'){
                                Intent intent = new Intent(RegistrationActivity.this,MainActivity.class);
                                AuthorSettings.nick = nick.getText().toString();
                                writeToFile();
                                startActivity(intent);
                            }
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            }
        });
        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String answer = checkData(1,nick.getText().toString(),convPass(pass.getText().toString()));
                            if(answer.charAt(0) == 's'){
                                Intent intent = new Intent(RegistrationActivity.this,MainActivity.class);
                                AuthorSettings.nick = nick.getText().toString();
                                writeToFile();
                                startActivity(intent);
                            }
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        }


                    }
                }).start();
            }
        });

    }

    private String convPass(String pass) throws NoSuchAlgorithmException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        byte[] bytes = md5.digest(pass.getBytes());
        return new String(bytes,StandardCharsets.UTF_8);
    }

    private String checkData(int mode,String nick,String pass){
        try {
            Socket s = new Socket(IpConfig.ip,2283);
            PrintWriter pw = new PrintWriter(new OutputStreamWriter(s.getOutputStream(),"UTF-8"));
            Scanner scan = new Scanner(s.getInputStream(),"UTF-8");
            pw.println(mode);
            pw.println(nick);
            pw.println(pass);
            pw.flush();
            String answer = null;
            if(scan.hasNextLine())
                answer = scan.nextLine();
            System.out.println(answer);
            scan.close();
            pw.close();
            return answer;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
    private void writeToFile(){
        try {
            PrintWriter pw = new PrintWriter(openFileOutput("reg.txt", MODE_PRIVATE));
            pw.println(nick.getText().toString());
            pw.println(convPass(pass.getText().toString()));
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void readFromFile(){
        String nick = null,pass = null;
        try {
            Scanner scanner = new Scanner(openFileInput("reg.txt"));
            if(scanner.hasNextLine()) {
                nick = scanner.nextLine();
            }else{
                return;
            }
            if(scanner.hasNextLine()) {
                pass = scanner.nextLine();
            }else{
                return;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }
        final String finalNick = nick;
        final String finalPass = pass;
        new Thread(new Runnable() {
            @Override
            public void run() {
                String answer = checkData(1, finalNick, finalPass);
                if(answer.charAt(0) == 's'){
                    Intent intent = new Intent(RegistrationActivity.this,MainActivity.class);
                    AuthorSettings.nick = finalNick;
                    startActivity(intent);

                }
            }
        }).start();

    }
}
