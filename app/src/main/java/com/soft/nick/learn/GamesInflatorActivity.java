package com.soft.nick.learn;

import android.content.Context;
import android.content.Intent;
import android.os.Debug;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import HelpClasses.IdWidgets;
import HelpClasses.Widget;
import SocketConnection.GamesSocket;
import SocketConnection.SocketAction;

public class GamesInflatorActivity extends AppCompatActivity {

    private int id;
    ArrayList<Widget> widgetArrayList;
    int howMany = 0;
    boolean block = false;
    GamesSocket gs;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        System.out.println("resultCode " + widgetArrayList.get(howMany).a);
        if(resultCode != RESULT_OK){
            widgetArrayList.add(new Widget(widgetArrayList.get(howMany).id,widgetArrayList.get(howMany).a,widgetArrayList.get(howMany).b));
        }
        howMany++;
        block = false;
        if(howMany == widgetArrayList.size()){
            Intent in = new Intent(GamesInflatorActivity.this,MainActivity.class);
            startActivity(in);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_games_inflator);
        id = getIntent().getExtras().getInt("id");
        try {
            gs = new GamesSocket(id,new SocketAction() {
                @Override
                public void OnFull() {
                    widgetArrayList = gs.widList;
                    System.out.println("herre " + widgetArrayList.size());
                    for(int i = 0;i < widgetArrayList.size();i++){
                        System.out.println("wal " + widgetArrayList.size());
                        switch (widgetArrayList.get(i).id) {
                            case IdWidgets.TEXT_TEXT:
                                Intent intent = new Intent(GamesInflatorActivity.this, Text_Text.class);
                                intent.putExtra("text",widgetArrayList.get(i).a);
                                intent.putExtra("des",widgetArrayList.get(i).b);
                                Log.i("gg",widgetArrayList.get(i).b);
                                startActivityForResult(intent,i);
                                block = true;
                                break;
                            case IdWidgets.TEXT_LETTERS:
                                Intent intent2 = new Intent(GamesInflatorActivity.this, Text_Letters.class);
                                intent2.putExtra("name",widgetArrayList.get(i).a);
                                startActivityForResult(intent2,i);
                                block = true;
                                break;
                            case IdWidgets.TEXT_ANSWERS:
                                Intent intent3 = new Intent(GamesInflatorActivity.this, Text_Answers.class);
                                intent3.putExtra("name",widgetArrayList.get(i).a);
                                intent3.putExtra("des",widgetArrayList.get(i).b);
                                startActivityForResult(intent3,i);
                                block = true;
                                break;
                            case IdWidgets.TEXT_IMAGE:
                                System.out.println("hereds");
                                Intent intent4 = new Intent(GamesInflatorActivity.this, Text_Image.class);
                                intent4.putExtra("name",widgetArrayList.get(i).a);
                                intent4.putExtra("des",widgetArrayList.get(i).b);
                                startActivityForResult(intent4,i);
                                block = true;
                                break;
                            }
                        while(block){
                            try {
                                Thread.sleep(10);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                },this);

            } catch (IOException e) {
                    e.printStackTrace();
            }
    }
}

