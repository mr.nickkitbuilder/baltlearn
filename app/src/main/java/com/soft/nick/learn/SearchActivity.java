package com.soft.nick.learn;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

import HelpClasses.GamesAdapter;
import HelpClasses.PreviewGamesValues;
import HelpClasses.RequestCodes;

public class SearchActivity extends AppCompatActivity {
    ImageButton searchButt;
    EditText searchEdit;
    RecyclerView searchRecycler;
    ArrayList<PreviewGamesValues> pgvList;
    GamesAdapter ad;
    FloatingActionButton addBut;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Bundle bundle = data.getExtras();
        if(requestCode == RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER && resultCode == RESULT_OK){
            pgvList = (ArrayList<PreviewGamesValues>) bundle.getSerializable("list");
            addBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent in = new Intent(SearchActivity.this, createActivity.class);
                    startActivity(in);
                }
            });
            searchRecycler.setHasFixedSize(true);
            LinearLayoutManager verticalKayoutManager = new LinearLayoutManager(getApplicationContext());
            searchRecycler.setLayoutManager(verticalKayoutManager);
            ad = new GamesAdapter(pgvList);
            searchRecycler.setAdapter(ad);
        }
    }

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        searchButt = findViewById(R.id.searchButton);
        searchEdit = findViewById(R.id.searchInput);
        searchRecycler = findViewById(R.id.searchRecycler);
        addBut = findViewById(R.id.searchMainButton);
        pgvList = new ArrayList<>();
        searchButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SearchActivity.this,MainSocketActivity.class);
                in.putExtra("mode",2);
                in.putExtra("key",searchEdit.getText().toString());
                startActivityForResult(in,RequestCodes.REQUEST_CODE_FOR_RECEVIEVE_GAMES_FROM_SERVER);
            }
        });
    }
}
