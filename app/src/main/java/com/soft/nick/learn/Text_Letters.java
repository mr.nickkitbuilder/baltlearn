package com.soft.nick.learn;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class Text_Letters extends AppCompatActivity {

    TextView finalRes;
    ArrayList<Button> word;
    LinearLayout buttContainer;
    String wordS;
    boolean troubles = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text__letters);
        wordS = getIntent().getStringExtra("name");

        word = new ArrayList<>();
        finalRes = findViewById(R.id.finalTL);
        buttContainer = findViewById(R.id.linearTL);

        ArrayList<Character> chL = new ArrayList<>();
        Random ran = new Random();
        for(int i = 0; i < wordS.length();i++){
            chL.add(wordS.charAt(i));
        }
        String oldS = "";
        for(int i = 0; i < wordS.length();i++){
            int oldC = ran.nextInt(wordS.length() - i);
            oldS += String.valueOf(chL.get(oldC));
            chL.remove(oldC);
        }
        for(int i = 0;i < wordS.length();i++){
            Button old = new Button(this);
            old.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.MATCH_PARENT));
            float density = getApplicationContext().getResources().getDisplayMetrics().density;
            old.setTextSize(density * 9);
            old.setText(String.valueOf(oldS.charAt(i)));
            old.setAllCaps(false);
            old.setTypeface(Typeface.MONOSPACE);
            old.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View v) {
                    Button old = (Button) v;
                    System.out.println("a:" + wordS.charAt(finalRes.getText().toString().length()) + " b:" + old.getText().toString().charAt(0));
                    if(wordS.charAt(finalRes.getText().toString().length())
                            == old.getText().toString().charAt(0))
                    {
                        finalRes.setText(finalRes.getText().toString()
                                + old.getText().toString().charAt(0));
                        old.setVisibility(View.INVISIBLE);
                        if(wordS.equals(finalRes.getText().toString())){
                            if(!troubles)
                                setResult(RESULT_OK);
                            else
                                setResult(RESULT_CANCELED);
                            finish();
                        }

                    }else{
                        troubles = true;
                    }
                }
            });
            word.add(old);
            buttContainer.addView(old);
        }

    }
}
