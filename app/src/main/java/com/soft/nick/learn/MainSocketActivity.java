package com.soft.nick.learn;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import HelpClasses.AuthorSettings;
import HelpClasses.RequestCodes;
import SocketConnection.MainActivitySocket;
import SocketConnection.SocketAction;

public class MainSocketActivity extends AppCompatActivity {
    MainActivitySocket mas;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_socket);
        int mode = getIntent().getExtras().getInt("mode");
        String key = getIntent().getExtras().getString("key");
        final int out = getIntent().getExtras().getInt("out");
        mas = new MainActivitySocket(new SocketAction() {
            @Override
            public void OnFull() {
                Intent in = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("list", mas.getPgvList());
                System.out.println("1 " + mas.getPgvList().size());
                in.putExtras(bundle);
                if(out > 0){
                    setResult(2,in);
                }else {
                    setResult(RESULT_OK, in);
                }
                finish();
            }
        },mode,key,out);
    }
}
