package com.soft.nick.learn;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;

import HelpClasses.RequestCodes;

public class Text_Image extends AppCompatActivity {
    Button button;
    TextView name;
    ImageView des;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text__image);
        button = findViewById(R.id.buttonT_I);
        name = findViewById(R.id.desT_I);
        des = findViewById(R.id.nameT_I);
        String path = getIntent().getExtras().getString("des");
        String nameStr = getIntent().getExtras().getString("name");
        name.setText(nameStr);
        File mFile= new File(getFilesDir().getAbsolutePath(), path);
        Bitmap bitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath());
        des.setImageBitmap(bitmap);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}
