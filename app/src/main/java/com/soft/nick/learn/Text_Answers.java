package com.soft.nick.learn;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

import HelpClasses.Check;

public class Text_Answers extends AppCompatActivity {
    LinearLayout layTA;
    TextView tTA;
    String[] bSplit;
    int res;
    boolean troubles = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text__answers);
        String a = getIntent().getStringExtra("name");
        String b = getIntent().getStringExtra("des");
        res = Check.getRight(b);
        b = b.substring(Check.getCountsOfDigits(res) + 1);
        layTA = findViewById(R.id.layoutTA);
        tTA = findViewById(R.id.textTA);
        tTA.setText(a);
        bSplit = b.split("/");
        ArrayList<String> Sold = new ArrayList<>();
        ArrayList<String> Srnd = new ArrayList<>();
        Random r = new Random();
        for(int i =0;i < bSplit.length;i++){
            Sold.add(bSplit[i]);
        }
        for(int i =0;i < bSplit.length;i++){
            int d = r.nextInt(Sold.size());
            Srnd.add(Sold.get(d));
            Sold.remove(d);
        }

        for(int i = 0;i < Srnd.size();i++) {
            Button old = new Button(this);
            old.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));
            float density = getApplicationContext().getResources().getDisplayMetrics().density;
            old.setTextSize(density * 10);
            old.setAllCaps(false);
            old.setText(String.valueOf(bSplit[i]));
            old.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button old = (Button) v;
                    if (bSplit[res].equals(old.getText().toString())) {
                        if(!troubles)
                            setResult(RESULT_OK);
                        else
                            setResult(RESULT_CANCELED);
                        finish();
                    }else{
                        troubles = true;
                    }

                }
            });
            layTA.addView(old);
        }
    }
}
